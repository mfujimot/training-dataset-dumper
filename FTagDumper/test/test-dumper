#!/usr/bin/env bash

set -Eeu


################################################
# mode-based switches
################################################

# config mapping
#
CFG_DIR=$(dirname $(readlink -e ${BASH_SOURCE[0]}))/../../configs
declare -A CONFIGS=(
    [open]=${CFG_DIR}/OpenDataset.json
    [pflow]=${CFG_DIR}/EMPFlow.json
    [slim]=${CFG_DIR}/EMPFlowSlim.json
    [truth]=${CFG_DIR}/EMPFlowTruth.json
    [truthjets]=${CFG_DIR}/AktTruthJets.json
    [fatjets]=${CFG_DIR}/FatJets.json
    [fatjets-muonCorr]=${CFG_DIR}/FatJets_muonCorr.json
    [hash]=${CFG_DIR}/EMPFlow_hash.json
    [softe]=${CFG_DIR}/EMPFlow_softlep.json
    [minimal]=${CFG_DIR}/minimal.json
    [athena]=${CFG_DIR}/EMPFlow.json
    [retag-lite]=${CFG_DIR}/EMPFlowRetagLite.json
    [retag-mc20]=${CFG_DIR}/pflow-systs/TRK_RES_D0_MEAS.json
    [retag-full]=${CFG_DIR}/pflow-systs/nominal.json
    [retag-syst]=${CFG_DIR}/pflow-systs/TRK_RES_D0_MEAS.json
    [genwt]=${CFG_DIR}/EMPFlowGeneratorSystematics.json
    [lrt]=${CFG_DIR}/LRT.json
    [upgrade]=${CFG_DIR}/upgrade_r24.json
    [trackless]=${CFG_DIR}/TracklessEMPFlow.json
    [trigger]=${CFG_DIR}/trigger_pflow.json
    [trigger-tau]=${CFG_DIR}/trigger_tau.json
    [trigger-emtopo]=${CFG_DIR}/trigger_emtopo.json
    [trigger-all]=${CFG_DIR}/trigger_all.json
    [trigger-mc21]=${CFG_DIR}/trigger_mc21.json
    [trigger-SampleA]=${CFG_DIR}/trigger_all_SampleA.json
    [trigger-data]=${CFG_DIR}/trigger_pflow_data.json
    [event-selection-ttbar-data]=${CFG_DIR}/EMPFlowDataWithSelection.json
    [event-selection-ttbar-mc]=${CFG_DIR}/EMPFlowMCWithSelection.json
    [multi]=${CFG_DIR}/multi.json
    [flow]=${CFG_DIR}/flow.json
    [calo]=${CFG_DIR}/EMTopo_Caloclusters.json
    [calo-hits]=${CFG_DIR}/EMTopo_CaloclustersHits.json
    [trigger-trackjet]=${CFG_DIR}/TriggerTrackJets.json
    [trigger-fatjet]=${CFG_DIR}/TriggerFatJets.json
    [trigger-hits]=${CFG_DIR}/trigger_hits.json
    [trigger-hitz-network]=${CFG_DIR}/trigger_hitz_network.json
    [jer]=${CFG_DIR}/jetm2_jer.json
    [neutral]=${CFG_DIR}/EMPFlow_all_flows.json
    [retag-fatjet]=${CFG_DIR}/FatJetsRetag.json
    [taucomp]=${CFG_DIR}/TauComp.json
    [blocks]=${CFG_DIR}/BlockDemo.json
    [smeared-tracks]=${CFG_DIR}/trigger_upgrade.json
    [gn3]=${CFG_DIR}/GN3_dev.json
    [displaced-jets]=${CFG_DIR}/DisplacedJets.json
)

# standard samples
TRIGGER_MC21_AOD=r13983/AOD.601229.e8453_e8455_s3873_s3874_r13983.pool.root
TRIGGER_AOD=r14634/AOD.601229.e8514_e8528_s4111_s4114_r14634.pool.root
OFFLINE_PHYSVAL=p5627/DAOD_PHYSVAL.410470.e6337_s3681_r13144_p5627.small.pool.root
OFFLINE_FTAG1=p6368/DAOD_FTAG1.601229.e8453_s3873_r13829_p6368.small.pool.root
OFFLINE_FTAG1_MC20=p6453/DAOD_FTAG1.410470.e6337_s3681_r13144_p6453.small.pool.root
OFFLINE_PHYS=p6266/DAOD_PHYS.601230.e8514_e8528_s4162_s4114_r15540_r15516_p6266.small.pool.root
LLP1=r15540/DAOD_LLP1.mc23_13p6TeV.604242.PhPy8_ggF_H125_a16a16_4b_ctau100_filtered.e8582_e8586_s4162_s4114_r15540.small.pool.root

declare -A DATAFILES=(
    [open]=${OFFLINE_FTAG1}
    [pflow]=${OFFLINE_FTAG1}
    [slim]=${OFFLINE_FTAG1}
    [truth]=${OFFLINE_FTAG1}
    [truthjets]=${OFFLINE_PHYSVAL}
    [fatjets]=${OFFLINE_FTAG1}
    [fatjets-muonCorr]=${OFFLINE_FTAG1}
    [hash]=${OFFLINE_FTAG1}
    [softe]=${OFFLINE_FTAG1}
    [minimal]=${OFFLINE_FTAG1}
    [athena]=${OFFLINE_FTAG1}
    [retag-lite]=${OFFLINE_FTAG1}
    [retag-mc20]=${OFFLINE_FTAG1_MC20}
    [retag-full]=${OFFLINE_FTAG1}
    [retag-syst]=${OFFLINE_FTAG1}
    [genwt]=${OFFLINE_FTAG1}
    [lrt]=${OFFLINE_FTAG1}
    [upgrade]=p5799/DAOD_FTAG1.601229.e8481_s4149_r14700_r14702_p5799.small.pool.root
    [trackless]=p6023/DAOD_FTAG1.800030.HITS.e8514_s4159.r15310.p6023.small.root
    [trigger]=${TRIGGER_AOD}
    [trigger-tau]=${TRIGGER_AOD}
    [trigger-emtopo]=${TRIGGER_AOD}
    [trigger-all]=${TRIGGER_AOD}
    [trigger-mc21]=${TRIGGER_MC21_AOD}
    [trigger-SampleA]=r15424/AOD.601229.e8514_e8528_s4162_s4114_r15424_tid38017958_00.small.pool.root
    [trigger-data]=p6241/AOD.00473225.r15590_r15591_p6241.small.pool.root
    [event-selection-ttbar-data]=p5785/DAOD_FTAG2.00451618.r14858_p5785_p6288.small.pool.root
    [event-selection-ttbar-mc]=p6285/DAOD_FTAG2.601230.e8514_s4159_r15530_p6285.small.pool.root
    [multi]=${OFFLINE_FTAG1}
    [flow]=${OFFLINE_FTAG1}
    [calo]=r14622/AOD.601229.e8514_s4162_r14622.small.pool.root
    [calo-hits]=${TRIGGER_AOD}
    [trigger-trackjet]=${TRIGGER_MC21_AOD}
    [trigger-fatjet]=r16269/AOD.802423.e8514_e8586_s4369_s4370_r16269.small.pool.root
    [trigger-hits]=${TRIGGER_AOD}
    [trigger-hitz-network]=${TRIGGER_AOD}
    [jer]=p5548/DAOD_JETM2.e8485_s3986_r14060_p5548.small.pool.root
    [neutral]=${OFFLINE_FTAG1}
    [retag-fatjet]=p5738/DAOD_FTAG1.601229.e8514_s4162_r14622_p5738.small.pool.root
    [taucomp]=${OFFLINE_PHYS}
    [blocks]=${OFFLINE_FTAG1}
    [smeared-tracks]=p5799/DAOD_FTAG1.601229.e8481_s4149_r14700_r14702_p5799.small.pool.root
    [gn3]=${OFFLINE_FTAG1}
    [displaced-jets]=${LLP1}
)
declare -A TESTS=(
    [open]=dump-single-btag
    [pflow]=dump-single-btag
    [slim]=dump-single-btag
    [truth]=dump-single-btag
    [truthjets]=dump-single-btag
    [fatjets]=dump-single-btag
    [fatjets-muonCorr]=dump-single-btag
    [hash]=dump-single-btag
    [softe]=dump-single-btag
    [minimal]=dump-minimal-btag
    [athena]=dump-single-btag
    [retag-lite]=dump-retag-lite
    [retag-mc20]=dump-retag
    [retag-full]=dump-retag
    [retag-syst]=dump-retag
    [genwt]=dump-single-btag
    [lrt]=dump-lrt
    [upgrade]=dump-single-btag
    [trackless]=dump-single-btag
    [trigger]=dump-trigger-pflow
    [trigger-tau]=dump-trigger-tau
    [trigger-emtopo]=dump-trigger-emtopo
    [trigger-all]=dump-trigger-all
    [trigger-mc21]=dump-trigger-all
    [trigger-SampleA]=dump-trigger-all
    [trigger-data]=dump-trigger-pflow
    [event-selection-ttbar-data]=dump-single-btag
    [event-selection-ttbar-mc]=dump-single-btag
    [multi]=dump-multi-config
    [flow]=dump-single-btag
    [calo]=dump-single-btag
    [calo-hits]=dump-single-btag
    [trigger-trackjet]=dump-trigger-trackjet
    [trigger-fatjet]=dump-trigger-fatjet
    [trigger-hits]=dump-trigger-emtopo
    [trigger-hitz-network]=dump-trigger-emtopo
    [jer]=dump-jer
    [neutral]=dump-single-btag
    [retag-fatjet]=dump-retag-fatjet
    [taucomp]=dump-single-btag
    [blocks]=dump-single-btag
    [smeared-tracks]=dump-smeared-tracks
    [gn3]=dump-single-btag
    [displaced-jets]=dump-displaced-jets
)

################################################
# parse arguments
################################################

ALL_MODES=${!CONFIGS[*]}

print-usage() {
    echo "usage: ${0##*/} [-${OPT_KEYS//:}] [-d <dir>] <mode>" 1>&2
}

usage() {
    print-usage
    exit 1;
}

help() {
    print-usage
    cat <<EOF

The ${0##*/} utility will download a test DAOD file and use it to
produce a training dataset.

Options:
 -e: extended help (print all modes)
 -d <dir>: specify directory to run in
 -r: build (and run on) a reduced test file
 -p: force dumping at full precision
 -a: enable floating point exception auditing
 -q: just get the input file, don't run the test
 -h: print help

If no -d argument is given we'll create one in /tmp and work there.

EOF
    if (( $# > 0 )) ; then
        cat <<EOF

Run modes:
$(dump-modes)

EOF
    fi
    exit 1
}

# Terminal magic: if stdout is set to go to a terminal we'll add
# colors to the help output below. Otherwise we strip this stuff
# out. Note that you have to check the interactivity here: within the
# function the output stream is never seen as going to the terminal.
if [[ -t 1 ]]; then
    INTERACTIVE=yes
else
    INTERACTIVE=""
fi
dump-modes() {
    if [[ ${INTERACTIVE} ]]; then
        local RB=$(tput setaf 1)$(tput bold)
        local NO=$(tput sgr0)
        local Y=$(tput setaf 3)
        local G=$(tput setaf 2)
        local FMT="  %s ${Y}-> ${RB}%s${NO} ${G}%s${NO} %s\n"
    else
        local FMT="  %s -> %s %s %s\n"
    fi
    local mode
    for mode in ${ALL_MODES[*]/fail}
    do
        local func=${TESTS[$mode]}
        local config=${CONFIGS[$mode]-$DEFAULT_CONFIG}
        local data=${DATAFILES[$mode]}
        printf "${FMT}" $mode "$func" ${config##*/} $data
    done
}


DIRECTORY=""
DATA_URL=https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/dumper-test-files/-/raw/main/
REDUCE_INPUT=""
FULL_PRECISION=""
AUDIT_FPE=""
QUIT_AFTER_INPUT_DOWNLOAD=""

OPT_KEYS=":ed:rpaqh"
dump-complete() {
    printf "%s " ${ALL_MODES[*]}
    local char
    echo -n ${OPT_KEYS//:} | while read -n 1 char
    do
        printf -- "-%s " $char
    done
}

# the c option is "hidden", we just use it to pass options to tab
# complete
while getopts ${OPT_KEYS}c o; do
    case "${o}" in
        d) DIRECTORY=${OPTARG} ;;
        r) REDUCE_INPUT=1 ;;
        p) FULL_PRECISION="-p" ;;
        a) AUDIT_FPE="--FPEAuditor" ;;
        q) QUIT_AFTER_INPUT_DOWNLOAD=1 ;;
        h) help ;;
        e) help extended ;;
        # this is just here for tab complete
        c) dump-complete; exit 0 ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))

if (( $# != 1 )) ; then
    usage
fi

MODE=$1


############################################
# Check that all the modes / paths exist
############################################
#
if [[ ! ${CONFIGS[$MODE]+x} ]]; then usage; fi
CFG=${CONFIGS[$MODE]}

if [[ ! ${DATAFILES[$MODE]+x} ]]; then usage; fi
DOWNLOAD_PATH=${DATAFILES[$MODE]}
FILE=${DOWNLOAD_PATH##*/}

if [[ ! ${TESTS[$MODE]+x} ]]; then usage; fi
RUN=${TESTS[$MODE]}


############################################
# Define the run command
############################################

# basic run command
function run-test-basic {
    local CMD="$RUN $FILE -c $CFG $FULL_PRECISION $AUDIT_FPE"
    echo "running ${CMD}"
    ${CMD}
}

# run on reduced input file
function run-test-reduced {
    local REDUCED=DAOD_SMALL.pool.root
    local REDCMD="make-test-file $FILE -c $CFG -o $REDUCED "
    echo "running ${REDCMD}"
    ${REDCMD}
    cat <<EOF

#################### successfully ran reduction ####################
now processing $REDUCED to HDF5
####################################################################

EOF
    local CMD="$RUN $FILE -c $CFG"
    echo "running ${CMD}"
    ${CMD}
}

if [[ ${REDUCE_INPUT} ]] ; then
    if [[ ${MODE} != "minimal" ]] ; then
        echo "Input reduction is not supported in '${MODE}' mode" >&2
        exit 1
    fi
    RUN_TEST=run-test-reduced
else
    RUN_TEST=run-test-basic
fi


#############################################
# now start doing stuff
#############################################
#
if [[ -z ${DIRECTORY} ]] ; then
    DIRECTORY=$(mktemp -d)
    echo "running in ${DIRECTORY}" >&2
fi

if [[ ! -d ${DIRECTORY} ]]; then
    if [[ -e ${DIRECTORY} ]] ; then
        echo "${DIRECTORY} is not a directory" >&2
        exit 1
    fi
    mkdir ${DIRECTORY}
fi
cd $DIRECTORY

# get files
if [[ ! -f ${FILE} ]] ; then
    echo "getting file ${FILE}" >&2
    curl -s ${DATA_URL}/${DOWNLOAD_PATH} > ${FILE}
fi

if [[ ${QUIT_AFTER_INPUT_DOWNLOAD} ]]; then
    echo "got ${FILE}, quitting"
    exit 0
fi

# now run the test
DUMPSTER_CI_TEST=1 ${RUN_TEST}

# require some jets were written
echo "========== output summary ==========="
test-output output.h5
