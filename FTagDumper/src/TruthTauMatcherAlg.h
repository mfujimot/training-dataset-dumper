/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRUTH_TAU_MATCHER_ALG_H
#define TRUTH_TAU_MATCHER_ALG_H

#include "VariableMule.hh"

#include "xAODBase/IParticleContainer.h"
#include "AthenaBaseComps/AthReentrantAlgorithm.h"

// TruthTauMatcherAlg class: Matches truth taus to reco. jets
class TruthTauMatcherAlg: public AthReentrantAlgorithm
{
public:
  TruthTauMatcherAlg(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize () override;
  virtual StatusCode execute (const EventContext&) const override;
  virtual StatusCode finalize () override;

private:
  using JC = xAOD::IParticleContainer;
  using IPL = ElementLink<xAOD::IParticleContainer>;
  using IPLV = std::vector<IPL>;

  // Variables to copy from truth taus to matched jets
  VariableMule<float,JC> m_floats{NAN};
  VariableMule<int,JC> m_ints{-1};
  VariableMule<uint,JC> m_uints{0};
  VariableMule<char,JC> m_chars{-1};
  VariableMule<IPLV,JC> m_iparticles{{}};

  SG::ReadHandleKey<JC> m_recoJets {this, "recoJets", "", "Reco. jet collection"};
  SG::ReadHandleKeyArray<JC> m_truthTaus {this, "truthTaus", {}, "Truth tau collection"};

  // Decorator properties (keys for storing match results)
  Gaudi::Property<std::string> m_dRKey {
    this, "dR", "deltaRToMatchedTau", "decorator for delta R to match"};
  Gaudi::Property<std::string> m_dPtKey {
    this, "dPt", "deltaPtToMatchedTau", "decorator for delta pt to match"};
  Gaudi::Property<std::string> m_linkKey {
    this, "particleLink", "", "decorator for matched IParticle"};
  Gaudi::Property<std::string> m_matchKey {
    this, "match", "jetIsMatched", "1 if matched, 0 if not"};
  // Matching logic configuration
  Gaudi::Property<float> m_maxDeltaR {
    this, "maxDeltaR", 0.3,
    "Maximum delta R for which it is still a match."
  };
  Gaudi::Property<float> m_minTruthTauPt {
    this, "minTruthTauPt", 0, "Set minimum pT value for truth taus"
  };

  // Write decorators to store results
  SG::WriteDecorHandleKey<JC> m_drDecorator;
  SG::WriteDecorHandleKey<JC> m_dPtDecorator;
  SG::WriteDecorHandleKey<JC> m_linkDecorator;
  SG::WriteDecorHandleKey<JC> m_matchDecorator;

  // Jet selection function
  using JV = std::vector<const xAOD::IParticle*>;
  std::function<const xAOD::IParticle*(const xAOD::IParticle*, const JV&)> m_tauSelector;
};

#endif
