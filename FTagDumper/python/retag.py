"""
Here we construct a retagging sequence that takes a few variables
Such as :
  the input track, jet collections
  list of tracking systematics to run over
  option to merge LRT and standard tracks
This allows the users to perform retagging with more flexibilities
"""

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from FTagDumper.trackUtil import LRTMerger, applyTrackSys

from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg
from BTagging.BTagTrackAugmenterAlgConfig import BTagTrackAugmenterAlgCfg
from BTagging.BTagConfig import BTagAlgsCfg, GetTaggerTrainingMap
from AthenaConfiguration.ComponentFactory import CompFactory

from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConfig import (
    InDetTrackTruthOriginToolCfg
)


def retagging(
        flags,
        merge=False,
        track_collection="InDetTrackParticles",
        jet_collection="",
        track_systs=[],
        decorate_track_truth=False,
        original_jet_collection=None
):

    acc = ComponentAccumulator()

    pvCol = "PrimaryVertices"
    jet_collection = jet_collection
    # When the jet systematic is applied, the input jets will become AntiKt4EMPFlowJetsSys. 
    muon_collection = "Muons" if track_collection == "InDetTrackParticles" else None

    # get list of taggers to run
    if original_jet_collection is None:
        original_jet_collection = jet_collection
    original_nosuffix = original_jet_collection.replace("Jets","")
    trainingMap = GetTaggerTrainingMap(flags, original_nosuffix)

    if track_systs:
        acc.merge(
            applyTrackSys(
                flags,
                track_systs,
                track_collection,
                jet_collection,
            )
        )
        track_collection = "InDetTrackParticles_Sys"

    # The decoration on tracks needs to happen before the track merging step
    # Decorate the standard tracks first
    acc.merge(
        BTagTrackAugmenterAlgCfg(
            flags,
            TrackCollection=track_collection,
            PrimaryVertexCollectionName=pvCol,
        )
    )

    # add some truth labels
    if decorate_track_truth:
        trackTruthOriginTool = acc.popToolsAndMerge(
            InDetTrackTruthOriginToolCfg(flags)
        )
        acc.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.TruthParticleDecoratorAlg(
                'TruthParticleDecoratorAlg',
                trackTruthOriginTool=trackTruthOriginTool
            )
        )
        acc.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.TrackTruthDecoratorAlg(
                f'TrackTruthDecoratorAlg_{track_collection}',
                trackContainer=track_collection,
                trackTruthOriginTool=trackTruthOriginTool
            )
        )

    if merge:
        # Decorate the LRT tracks
        acc.merge(
            BTagTrackAugmenterAlgCfg(
                flags,
                TrackCollection="InDetLargeD0TrackParticles",
                PrimaryVertexCollectionName=pvCol,
            )
        )

        # merge the LRT and standard tracks into a single view collection
        acc.merge(LRTMerger())
        track_collection = "InDetWithLRTTrackParticles"

    # rename containers to avoid clashes
    acc.merge(RetagRenameInputContainerCfg("_retag", original_nosuffix, tracksKey=track_collection))
    acc.merge(JetTagCalibCfg(flags))

    # add the main b-tagging config
    acc.merge(
        BTagAlgsCfg(
            flags,
            JetCollection=jet_collection,
            nnList=trainingMap,
            trackCollection=track_collection,
            muons=muon_collection,
            AddedJetSuffix = ''
    ))

    return acc

# NOTE: this is copied from BTagging/BTagConfig.py, remove it soonish!
#
# This contains fixes that had to go into https://cern.ch/u3k2a,
# please remove it once we're using a release that includes it! That
# should happen in 25.2.17.
def RetagRenameInputContainerCfg(suffix, JetCollectionShort, tracksKey='InDetTrackParticles', addRenameMaps=None):
    acc=ComponentAccumulator()
    remapSvc = CompFactory.AddressRemappingSvc("AddressRemappingSvc")
    jc = JetCollectionShort
    s = suffix
    tc = tracksKey
    jac = 'xAOD::JetAuxContainer'
    bc = 'xAOD::BTaggingContainer'
    bac = 'xAOD::BTaggingAuxContainer'
    tpac = 'xAOD::TrackParticleAuxContainer'
    remapSvc.TypeKeyRenameMaps += [
        f'{jac}#{jc}Jets.jetFoldHash->{jc}Jets.jetFoldHash_{s}',
        f'{jac}#{jc}Jets.jetFoldHash_noHits->{jc}Jets.jetFoldHash_noHits_{s}',
        f'{jac}#{jc}Jets.jetFoldRankHash->{jc}Jets.jetFoldRankHash_{s}',
        f'{jac}#{jc}Jets.BTagTrackToJetAssociator->{jc}Jets.BTagTrackToJetAssociator_{s}',
        f'{jac}#{jc}Jets.JFVtx->{jc}Jets.JFVtx_{s}',
        f'{jac}#{jc}Jets.JFVtxFlip->{jc}Jets.JFVtxFlip_{s}',
        f'{jac}#{jc}Jets.SecVtx->{jc}Jets.SecVtx_{s}',
        f'{jac}#{jc}Jets.SecVtxFlip->{jc}Jets.SecVtxFlip_{s}',
        f'{jac}#{jc}Jets.btaggingLink->{jc}Jets.btaggingLink_{s}',
        f'{bc}#BTagging_{jc}->BTagging_{jc}_{s}',
        f'{bac}#BTagging_{jc}Aux.->BTagging_{jc}_{s}Aux.',
        f'xAOD::VertexContainer#BTagging_{jc}SecVtx->BTagging_{jc}SecVtx_{s}',
        f'xAOD::VertexAuxContainer#BTagging_{jc}SecVtxAux.->BTagging_{jc}SecVtx_{s}Aux.',
        f'xAOD::BTagVertexContainer#BTagging_{jc}JFVtx->BTagging_{jc}JFVtx_{s}'
        f'xAOD::BTagVertexAuxContainer#BTagging_{jc}JFVtxAux.->BTagging_{jc}JFVtx_{s}Aux.',
        f'{tpac}#{tc}.TrackCompatibility->{tc}.TrackCompatibility_{s}',
        f'{tpac}#{tc}.btagIp_d0->{tc}.btagIp_d0_{s}',
        f'{tpac}#{tc}.btagIp_z0SinTheta->{tc}.btagIp_z0SinTheta_{s}',
        f'{tpac}#{tc}.btagIp_d0Uncertainty->{tc}.btagIp_d0Uncertainty_{s}',
        f'{tpac}#{tc}.btagIp_z0SinThetaUncertainty->{tc}.btagIp_z0SinThetaUncertainty_{s}',
        f'{tpac}#{tc}.btagIp_trackMomentum->{tc}.btagIp_trackMomentum_{s}',
        f'{tpac}#{tc}.btagIp_trackDisplacement->{tc}.btagIp_trackDisplacement_{s}',
        f'{tpac}#{tc}.btagIp_invalidIp->{tc}.btagIp_invalidIp_' + '_retag',
        f'{tpac}#{tc}.JetFitter_TrackCompatibility_antikt4empflow->{tc}.JetFitter_TrackCompatibility_antikt4empflow_{s}',
        f'{jac}#{jc}Jets.TracksForBTagging->{jc}Jets.TracksForBTagging{s}',
        f'{jac}#{jc}Jets.TracksForBTaggingOverPtThreshold->{jc}Jets.TracksForBTaggingOverPtThreshold{s}',
        f'{jac}#{jc}Jets.MuonsForBTagging->{jc}Jets.MuonsForBTagging{s}',
        f'{jac}#{jc}Jets.MuonsForBTaggingOverPtThreshold->{jc}Jets.MuonsForBTaggingOverPtThreshold{s}',
    ]

    # add extra mappings if present
    if addRenameMaps:
        remapSvc.TypeKeyRenameMaps += addRenameMaps

    acc.addService(remapSvc)
    acc.addService(
        CompFactory.ProxyProviderSvc(
            ProviderNames = [ "AddressRemappingSvc" ]
        )
    )

    return acc
