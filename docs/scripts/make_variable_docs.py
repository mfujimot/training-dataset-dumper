#!/usr/bin/env python3

"""
Generate documentation for a given HDF5 file
"""
_spec_help = 'Yaml variable specification file'

from argparse import ArgumentParser
from sys import stdout, stderr
from os.path import join
from itertools import chain
from pathlib import Path
import json


RESERVED = {'description', 'old_name', 'rank'}

CATEGORIES = {'jets': 'Jet variables', 'tracks': 'Track variables', 'truth_fromBC': 'Truth variables', 'truth_hadrons' : "Truth variables", "eventwise" : 'Event variables'}

ALWAYS_SHOW = {'pt', 'eta', 'mass'}
IGNORE = {'truth_leptons', 'ConeExclFinalLabels'}

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('input_file', type=Path)
    parser.add_argument('-s', '--spec-file', default=join('data', 'field-descriptions.yaml'),
                        help=_spec_help)
    output = parser.add_mutually_exclusive_group(required=False)
    output.add_argument('-m', '--markdown-output', action='store_true')
    output.add_argument('-y', '--yaml-output', action='store_true')
    parser.add_argument('-p', '--preamble', default=join('data', 'pflow_preamble.md'))
    parser.add_argument('-e', '--epilogue', default=join('data', 'pflow_epilogue.md'))
    parser.add_argument('-o', '--output', nargs='?', const='vars_pflow.md')
    return parser.parse_args()


def run():
    args = get_args()

    import yaml

    if args.input_file.suffix in {'.json'}:
        with open(args.input_file) as jfile:
            fields = get_dict_fields(json.load(jfile))
    else:
        from h5py import File
        with File(args.input_file, 'r') as h5:
            fields = get_h5_fields(h5)

    with open(args.spec_file) as spec_file:
        spec = yaml.safe_load(spec_file)
        add_meta(fields, spec)

    if args.yaml_output:
        stdout.write(yaml.dump(fields, default_flow_style=False))
    else:
        if not args.output:
            out_file = stdout
        else:
            out_file = open(args.output, 'w')

        with open(args.preamble) as pre:
            for line in pre:
                out_file.write(line)
        write_markdown(fields, out_file)
        with open(args.epilogue) as epe:
            for line in epe:
                out_file.write(line)

FOOTNOTES = {
    'bjes': (
        "In production, flavor tagging algoirhtms run before the final jet calibration is applied."
        "The jet energy scale at this stage is set by the jet calibration which is applied when jets are built."
        "Jets saved in DAODs are calibrated to this scale as well."
        "For consistency, we use this jet energy scale in the training as well."
    ),
    'truth_displacement': (
        "For truth variables the displacement is defined as the decay"
        " vertex position, relative to the production vertex for "
        "the simulated event."
    ),
    'ip': (
        r"The conventional $d_0$ takes the sign of"
        r" $(\vec{p}_0 \times \vec{d}_0) \cdot \hat{z}$,"
        r" where $\vec{d}_0$ is the transverse track displacement at the"
        r" closest approach to the primary vertex and $\vec{p}_0$ is"
        r" the momentum at that point."
    ),
    'lifetime_signed': (
        r"The lifetime signed impact parameters are given a positive sign"
        r" if $|\phi_0 - \phi_{\mathrm{jet}}| < \pi / 2$ and a negative"
        r" sign otherwise, where $\phi_0$ and $\phi_{\mathrm{jet}}$ are"
        r" the $\phi$ components of the track displacement and the"
        r" jet momentum, respectively."
    ),
    'high_level': (
        "This is a high-level output from a neural network."
        " It should not be considered a stable input to design new taggers."
    ),
    'dl1_legacy': (
        "The DL1 series includes DL1, DL1r, and DL1d. "
        "These are legacy taggers which are not recommended for physics"
        " analysis. They are included here only for comparison."
    ),
    'track_origin': (
        r"Tracks labels: 0 = pileup, 1 = fake, 2 = prompt,"
        r" 3 = from $b$-hadron, 4 = from $c$ child of a $b$ hadron,"
        r" 5 = from $c$-hadron, 6 = from $\tau$ lepton,"
        r" 7 = from other secondary decay."
        " See [InDetTrackTruthOriginDefs](https://gitlab.cern.ch/atlas/athena/-/blob/release/25.2.34/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h?ref_type=tags#L159-169) for ore information."
    )
}


def rank_key(item):
    key, fields = item
    if 'rank' in fields:
        return (fields['rank'], key)
    else:
        return (1, key)


def write_markdown(fields, outfile, depth=0, ignore=set()):
    sep = '-'
    all_notes = set()

    for name, sub in fields.items():
        if 'description' in sub:
            desc = sub['description']
            if desc == "":
                desc, rank = infer_description(name)
                sub['description'] = desc
                sub['rank'] = rank
        if notes := sub.get('notes'):
            sub['description'] += ' '.join(f'[^{x}]' for x in notes)
            all_notes |= set(notes)

    sorted_fields = sorted(fields.items(), key=rank_key)

    tab_header = """\n|Variable Name|Description|\n|--------|-----------|\n"""

    for name, sub in sorted_fields:

        if 'description' in sub:
            desc = sub['description']
            print_conditions = [
                name not in ignore,
                sub.get('always_show'),
                name in ALWAYS_SHOW]

            if any(print_conditions):
                outfile.write(f'|`{name}`|{desc}|\n')

        else:
            if name in IGNORE:
                continue
            elif name in CATEGORIES.keys():
                outfile.write(f'### {CATEGORIES[name]}\n')
                outfile.write(f'{tab_header}')
            else:
                outfile.write(f'{sep:>{depth}} **{name}:**\n')
            write_markdown(sub, outfile, depth+2, ignore)

    outfile.write('\n')

    for note in all_notes:
        outfile.write(f'[^{note}]: {FOOTNOTES[note]}\n')

    outfile.write('\n')


def infer_description(name):
    # high level tagger scores
    hlt_strings = ['dips', 'DL1', 'UMAMI', 'GN1', 'GN2']
    p_strings = ['_pb', '_pc', '_pu', '_ptau']
    if any([s in name for s in hlt_strings]) and any([s in name for s in p_strings]):
        return "**High level tagger output!** Do not use for training!", 999

    # flip tagger scores
    flip_strings = ['Flip_', 'flip']
    if any([s in name for s in flip_strings]):
        return "Flip tagger output, used for calibration studies", 500

    # soft muon
    smt_strings = ['softMuon_']
    if any([s in name for s in smt_strings]):
        return "Soft muon tagger output", 400

    return "", 1


def add_meta(fields, spec):
    new_fields = {}
    for n, old in fields.items():
        if n not in spec:
            pass
            #stderr.write(f'{n} not found in {spec}, using default\n')
        elif isinstance(old, str) and not old:
            new_fields[n] = ' '.join(spec[n].split())
        else:
            add_meta(old, spec[n])

    for n, new in new_fields.items():
        fields[n] = new

    for meta in ['rank', 'always_show', 'notes']:
        if meta in spec:
            fields[meta] = spec[meta]


def get_h5_fields(h5):
    try:
        fields = {}
        for n in h5.dtype.names:
            attribs = {'description': ''}
            fields[n] = attribs
        return fields
    except AttributeError as err:
        if "no attribute 'dtype'" not in err.args[0]:
            raise

    return {s.name.split('/')[-1]: get_h5_fields(s) for s in h5.values()}


def _get_leaves(struct, depth=1, skip=set()):
    if depth == 0:
        return {x: {'description': ''} for x in struct}
    ret = {}
    for name, sub in struct.items():
        if name not in skip:
            ret |= _get_leaves(sub, depth-1)
    return ret


def get_dict_fields(yaml_file):
    track = {}
    for track_cfg in yaml_file['tracks']:
        track |= _get_leaves(track_cfg['variables'])
    truth = {}
    for truth_cfg in yaml_file['truths']:
        if varaibles := truth_cfg.get('vars'):
            truth |= _get_leaves(varaibles)
    jet = {}
    for sub in 'jet', 'btagging', 'event':
        jet |= _get_leaves(yaml_file['variables'][sub])
    eventwise_name = 'eventwise'
    eventwise = _get_leaves(yaml_file['variables'][eventwise_name])
    return {
        'jets': jet,
        'tracks': track,
        'truth_hadrons': truth,
        eventwise_name: eventwise
    }


if __name__ == '__main__':
    run()
