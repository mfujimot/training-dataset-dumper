variables:
  ATHANALYSIS_IMAGE: gitlab-registry.cern.ch/atlas/athena/athanalysis:25.2.36
  ATHENA_IMAGE: gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos-dev

default:
  image: $ATHANALYSIS_IMAGE
  retry: 1
  before_script:
    - set +e
    - source ~/release_setup.sh
    - source setup/allow-breaking-edm.sh
    - if [[ -d build ]]; then source build/**/setup.sh; fi

stages:
  - docker
  - build
  - run
  - pages


###################################
## build images
###################################
build_img:
  stage: docker
  allow_failure: true
  image:
    # The kaniko debug image is recommended because it has a shell, and a shell is required for an image to be used with GitLab CI/CD.
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    IMAGE_DESTINATION: $CI_REGISTRY_IMAGE:latest
  before_script: ""
  script:
    # Build and push the image from the Dockerfile at the root of the project.
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $IMAGE_DESTINATION
    # Print the full registry path of the pushed image
    - echo "Image pushed successfully to ${IMAGE_DESTINATION}"
  rules: # don't worry about this in MRs to speed things up
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH=="atlas-flavor-tagging-tools/algorithms/training-dataset-dumper"
      when: always

build_img_tag:
  extends: build_img
  variables:
    IMAGE_DESTINATION: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  only:
    - tags
  rules: null


###################################
## compile jobs images
###################################
## Compile with AthAnalysis
compile_athanalysis:
  stage: build
  script:
    - mkdir -p build && cd build
    - cmake .. | tee cmake.log
    - make clean # make sure we don't have residual compilation results
    - make -j $(nproc) 2>&1 | tee -a cmake.log # dump the log files
  artifacts:
    paths:
      - build
  image: $ATHANALYSIS_IMAGE

## Compile with Athena
compile_athena:
  stage: build
  extends: compile_athanalysis
  tags: [k8s-cvmfs]
  before_script:
    - set +e
    - source setup/athena.sh
    - if [[ -d build ]]; then source build/**/setup.sh; fi
  #
  # TODO:
  #
  # this `script` should be removed eventually, we can use the same
  # script that comes from the AthAnalysis build, but for some reason
  # we were running out of memory (?) and terminating the CI builds in
  # Athena. The workaround here is to divide the procs by 2.
  #
  # See https://cern.ch/dumper154
  #
  script:
    - mkdir -p build && cd build
    - cmake .. | tee cmake.log
    - make clean # make sure we don't have residual compilation results
    - make -j $(( $(nproc) / 2 )) 2>&1 | tee -a cmake.log # dump the log files
  image: $ATHENA_IMAGE


###################################
## run tests
###################################

# AthAnalysis
.run_athanalysis: &run-athanalysis
  image: $ATHANALYSIS_IMAGE
  stage: run
  needs: [compile_athanalysis]

test_pflow:
  <<: *run-athanalysis
  script:
    - test-dumper -d pflow_reduced pflow
  artifacts:
    paths:
      - pflow_reduced
    expire_in: 1 hour

test_pflow_full:
  <<: *run-athanalysis
  script:
    - test-dumper -p -d pflow_full pflow
  artifacts:
    paths:
      - pflow_full
    expire_in: 1 hour

test_slim:
  <<: *run-athanalysis
  script:
    - test-dumper slim

test_truth:
  <<: *run-athanalysis
  script:
    - test-dumper -d $PWD/ci-pflow-truth truth
  artifacts:
    paths:
      - ci-pflow-truth
    expire_in: 1 hour

test_truthjets:
  <<: *run-athanalysis
  script:
    - test-dumper truthjets

test_trackless:
  <<: *run-athanalysis
  script:
    - test-dumper trackless

test_event_selection_ttbar_data:
  <<: *run-athanalysis
  script:
    - test-dumper event-selection-ttbar-data

test_event_selection_ttbar_mc:
  <<: *run-athanalysis
  script:
    - test-dumper event-selection-ttbar-mc

test_minimal:
  <<: *run-athanalysis
  script:
    - test-dumper minimal

test_reduced:
  <<: *run-athanalysis
  script:
    - test-dumper -r minimal

test_multi:
  <<: *run-athanalysis
  script:
    - test-dumper multi

test_flow:
  <<: *run-athanalysis
  script:
    - test-dumper flow

test_trigger_emtopo:
  <<: *run-athanalysis
  script:
    - test-dumper -d trig_reduced trigger-emtopo
  artifacts:
    paths:
      - trig_reduced
    expire_in: 1 hour

test_trigger_emtopo_full:
  <<: *run-athanalysis
  script:
    - test-dumper -p -d trig_full trigger-emtopo
  artifacts:
    paths:
      - trig_full
    expire_in: 1 hour

test_trigger_hits:
  <<: *run-athanalysis
  script:
    - test-dumper trigger-hits

test_trigger_hitz_network:
  <<: *run-athanalysis
  script:
    - test-dumper trigger-hitz-network

test_softe:
  <<: *run-athanalysis
  script:
    - test-dumper softe

test_fatjet:
  <<: *run-athanalysis
  script:
    - test-dumper fatjets

test_fatjet_muonCorr:
  <<: *run-athanalysis
  script:
    - test-dumper fatjets-muonCorr

test_jer:
  <<: *run-athanalysis
  script:
    - test-dumper jer

test_tau:
  <<: *run-athanalysis
  script:
    - test-dumper taucomp

test_retag_lite:
  <<: *run-athanalysis
  script:
    - test-dumper retag-lite

test_genwt:
  <<: *run-athanalysis
  script:
    - test-dumper genwt

test_blocks:
  <<: *run-athanalysis
  script:
    - test-dumper blocks

test_hash:
  <<: *run-athanalysis
  script:
    - test-dumper hash

test_neutrals:
  <<: *run-athanalysis
  script:
    - test-dumper neutral

test_calo:
  <<: *run-athanalysis
  script:
    - test-dumper calo

test_calo_hits:
  <<: *run-athanalysis
  script:
    - test-dumper calo-hits

test_open:
  <<: *run-athanalysis
  script:
    - test-dumper open

test_gn3:
  <<: *run-athanalysis
  script:
    - test-dumper gn3

test_displaced_jets:
  <<: *run-athanalysis
  script:
    - test-dumper displaced-jets

test_open:
  <<: *run-athanalysis
  script:
    - test-dumper -d $PWD/ci-open open
  artifacts:
    paths:
      - ci-open
    expire_in: 1 hour


# Athena
.run_athena: &run-athena
  image: $ATHENA_IMAGE
  tags: [k8s-cvmfs]
  stage: run
  needs: [compile_athena]
  before_script:
    - set +e
    - source setup/athena.sh
    - if [[ -d build ]]; then source build/**/setup.sh; fi

test_athena:
  <<: *run-athena
  script:
    - test-dumper -p athena

test_retag_full:
  <<: *run-athena
  script:
    - test-dumper retag-full

test_retag_mc20:
  <<: *run-athena
  script:
    - test-dumper retag-mc20

test_retag_syst:
  <<: *run-athena
  script:
    - test-dumper retag-syst

test_lrt:
  <<: *run-athena
  script:
    - test-dumper lrt

test_trigger:
  <<: *run-athena
  script:
    - test-dumper trigger

test_trigger_all:
  <<: *run-athena
  script:
    - test-dumper trigger-all

test_trigger_mc21:
  <<: *run-athena
  script:
    - test-dumper trigger-mc21

test_trigger_SampleA:
  <<: *run-athena
  script:
    - test-dumper trigger-SampleA

test_trigger_data:
  <<: *run-athena
  script:
    - test-dumper trigger-data

test_upgrade:
  <<: *run-athena
  script:
    - test-dumper upgrade

test_smeared_tracks:
  <<: *run-athena
  script:
    - test-dumper smeared-tracks

test_trigger_trackjet:
  <<: *run-athena
  script:
    - test-dumper trigger-trackjet

test_trigger_fatjet:
  <<: *run-athena
  script:
    - test-dumper trigger-fatjet

test_trigger_tau:
  <<: *run-athena
  script:
    - test-dumper trigger-tau

test_ca_retag_fatjet:
  <<: *run-athena
  script:
    - test-dumper retag-fatjet

# various unit tests
test_submit:
  stage: run
  tags: [k8s-cvmfs]
  needs: [compile_athanalysis]
  script:
    # git seems to get angry about multiple users modifying files,
    # this "safe.directory" is here to avoid a "dubious ownership"
    # error
    - git config --global --add safe.directory '*'
    - export USER=ci-test
    - source FTagDumper/grid/setup.sh dry-run
    - grid-submit -d -f single-btag

test_configs:
  stage: run
  needs: [compile_athanalysis]
  script:
    - test-configs-single-b -v


###################################
## compare outputs
###################################
compare_precision_pflow:
  stage: run
  needs: [test_pflow_full, test_pflow]
  script:
    - h5diff -v -d 0.0003  pflow_full/output.h5 pflow_reduced/output.h5 /jets/
    - h5diff -v -p 0.001  pflow_full/output.h5 pflow_reduced/output.h5 /tracks/

compare_precision_trigger:
  stage: run
  needs: [test_trigger_emtopo_full, test_trigger_emtopo]
  script: h5diff -p 0.0005 trig_full/output.h5 trig_reduced/output.h5


###################################
## build documentation
###################################
pages:
  tags: [k8s-cvmfs]
  image: python:3.11
  stage: pages
  needs: [test_truth, test_open]
  before_script: "" # overwrite default, do nothing
  script:
    - python docs/scripts/ca_block_docs.py
    - docs/scripts/ci-dump-variable-docs pflow ci-pflow-truth/output.h5
    - docs/scripts/ci-dump-variable-docs open ci-open/output.h5
    - docs/scripts/ci-mkdocs
  artifacts:
    paths:
      - public
    expire_in: 1 hour
